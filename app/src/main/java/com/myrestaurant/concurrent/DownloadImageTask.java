package com.myrestaurant.concurrent;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import com.myrestaurant.R;

import java.io.InputStream;
import java.lang.ref.WeakReference;

/**
 * Created by vortex on 7/13/16.
 */
public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
    private WeakReference<ImageView> imageViewWeakReference;
    private Bitmap image;

    public DownloadImageTask(WeakReference<ImageView> imageViewWeakReference) {
        this.imageViewWeakReference = imageViewWeakReference;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if(this.imageViewWeakReference.get() != null)
            this.imageViewWeakReference.get().setImageResource(R.drawable.placeholder);
    }

    protected Bitmap doInBackground(String... urls) {
        String urldisplay = urls[0];
        try {
            InputStream in = new java.net.URL(urldisplay).openStream();
            image = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            image = null;
        }
        return image;
    }

    @SuppressLint("NewApi")
    protected void onPostExecute(Bitmap result) {
        if (result != null) {
            if(this.imageViewWeakReference.get() != null)
                imageViewWeakReference.get().setImageBitmap(result);
        }
    }
}
