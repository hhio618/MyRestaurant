package com.myrestaurant.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vortex on 7/2/16.
 */
@org.parceler.Parcel
public class Food{
    private int id;
    @SerializedName("name")
    private String food_name;
    @SerializedName("description")
    private String food_description;

    private String extra_url;


    public String getExtra_url() {
        return extra_url;
    }

    public void setExtra_url(String extra_url) {
        this.extra_url = extra_url;
    }

    public void setFood_name(String food_name) {
        this.food_name = food_name;
    }

    public void setFood_description(String food_description) {
        this.food_description = food_description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    private String icon;

    public Food(String food_name, String food_description, String icon, String url) {
        this.food_name = food_name;
        this.food_description = food_description;
        this.icon = icon;
        this.extra_url = url;
    }

    public Food() {

    }

    public String getFood_description() {
        return food_description;
    }


    public String getFood_name() {
        return food_name;
    }

//    @Override public int describeContents() {
//        return 0;
//    }

//    public Food(Parcel in) {
//        this.food_name = in.readString();
//        this.food_description = in.readString();
//        this.extra_url = in.readString();
//        this.icon = in.readInt();
//    }

//    @Override
//    public void writeToParcel(Parcel dest, int flags) {
//        dest.writeString(this.food_name);
//        dest.writeString(this.food_description);
//        dest.writeString(this.extra_url);
//        dest.writeInt(this.icon);
//    }
//    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
//        public Food createFromParcel(Parcel in) {
//            return new Food(in);
//        }
//
//        public Food[] newArray(int size) {
//            return new Food[size];
//        }
//    };
}
