package com.myrestaurant.rxbus;


import android.app.Activity;
import android.support.v4.app.Fragment;

import com.trello.rxlifecycle.ActivityLifecycleProvider;
import com.trello.rxlifecycle.FragmentLifecycleProvider;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.subjects.PublishSubject;
import rx.subjects.SerializedSubject;
import rx.subjects.Subject;

/**
 * Created by hhio618 on 7/4/16.
 */
// this is the middleman object
public class RxBus {

    private static final RxBus INSTANCE = new RxBus();

    public static RxBus getInstance() {
        return INSTANCE;
    }

    private final Subject<Object, Object> mBusSubject = new SerializedSubject<>(PublishSubject.create());

    public <T> Subscription register(final Class<T> eventClass, ActivityLifecycleProvider lifecycleProvider, Action1<T> action) {
        return createRegisterObservable(eventClass)
                .compose(lifecycleProvider.<T>bindToLifecycle())
                .observeOn(AndroidSchedulers.from(((Activity) lifecycleProvider).getMainLooper()))
                .subscribe(action);
    }

    public <T> Subscription register(final Class<T> eventClass, FragmentLifecycleProvider lifecycleProvider, Action1<T> action) {
        return createRegisterObservable(eventClass)
                .compose(lifecycleProvider.<T>bindToLifecycle())
                .observeOn(AndroidSchedulers.from(((Fragment) lifecycleProvider).getActivity().getMainLooper()))
                .subscribe();
    }

    private <T> Observable<T> createRegisterObservable(final Class<T> eventClass) {

        return mBusSubject
                .filter(new Func1<Object, Boolean>() {
                    @Override public Boolean call(Object o) {
                        return o.getClass().equals(eventClass);
                    }
                })
                .map(new Func1<Object, T>() {
                    @Override public T call(Object o) {
                        return (T) o;
                    }
                });
    }


    public void post(Object o) {
        mBusSubject.onNext(o);
    }


}
