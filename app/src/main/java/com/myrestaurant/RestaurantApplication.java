package com.myrestaurant;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.facebook.stetho.Stetho;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by hhio618 on 7/4/16.
 */
public class RestaurantApplication extends Application {
    private static Context context;

    public static Context getContext() {
        return context;
    }
    public static boolean isNetworkConnectionAvailable() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        if (info == null) return false;
        NetworkInfo.State network = info.getState();
        return (network == NetworkInfo.State.CONNECTED || network == NetworkInfo.State.CONNECTING);
    }
    @Override public void onCreate() {
        super.onCreate();

        context = getApplicationContext();
        Stetho.initializeWithDefaults(this);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("font/bgb.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }


}
