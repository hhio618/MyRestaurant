package com.myrestaurant.exceptions;

/**
 * Created by vortex on 7/13/16.
 */
public class FloatingButtonClickedException extends  Exception {
    public FloatingButtonClickedException(String s){
        super(s);
    }
}
