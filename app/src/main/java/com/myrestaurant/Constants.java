package com.myrestaurant;

/**
 * Created by vortex on 7/2/16.
 */
public class Constants {
    public static final String INSTANCE_VAR = "instance_variable";
    public static final String FOOD_NAME = "food_name";
    public static final String FOOD_DESC = "desc";
    public static final String FOOD_ICON = "icon";
    public static final String FOOD_OBJECT = "food";
    public static final String WEB_URL = "url";
    public static final int API_DATA_LIMIT = 20;
    public  enum  RecyclerViewType{
        DEFAULT,
        PROGRESS
    }
}
