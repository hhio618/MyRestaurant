package com.myrestaurant.event;

import com.myrestaurant.model.Food;

/**
 * Created by hhio618 on 7/4/16.
 */
public class CreateDetailsFragmentEvent {
    private final Food food;

    public Food getFood() {
        return food;
    }

    public CreateDetailsFragmentEvent(Food food) {
        this.food = food;
    }
}
