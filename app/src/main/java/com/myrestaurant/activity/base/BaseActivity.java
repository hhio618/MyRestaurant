package com.myrestaurant.activity.base;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.View;

import com.myrestaurant.R;
import com.trello.rxlifecycle.components.support.RxAppCompatActivity;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by hhio618 on 7/4/16.
 */




public abstract class BaseActivity extends RxAppCompatActivity {



    private static final AppBarLayout.Behavior.DragCallback enabledDragCallback = new AppBarLayout.Behavior.DragCallback() {
        @Override public boolean canDrag(@NonNull AppBarLayout appBarLayout) {
            return true;
        }
    };
    private static final AppBarLayout.Behavior.DragCallback disabledDragCallback = new AppBarLayout.Behavior.DragCallback() {
        @Override public boolean canDrag(@NonNull AppBarLayout appBarLayout) {
            return false;
        }
    };
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @BindView(R.id.appbar) AppBarLayout appBarLayout;


    //    @Override
    //    public void setContentView(int layoutResId) {
    //        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
    //        View mLayout = inflater.inflate(layoutResId, null);
    //        LeftMenuDrawer.addView(mLayout,0);
    //        super.setContentView(LeftMenuDrawer);
    //    }
    //    @Override
    //    public View findViewById(int id) {
    //        View v = super.findViewById(id);
    //        return v;
    //
    //    }




    void shareApp() {
        final PackageManager pm = BaseActivity.this.getPackageManager();
        //get a list of installed apps.
        ApplicationInfo packageInfo = null;
        try {
            packageInfo = pm.getApplicationInfo("vortex.jokbazaar", 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        Log.d("", "Installed package :" + packageInfo.packageName);
        Log.d("", "Apk file path:" + packageInfo.sourceDir);
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("application/vnd.android.package-archive");
        i.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(packageInfo.sourceDir)));
        BaseActivity.this.startActivity(Intent.createChooser(i, "فرستادن فایل نصبی..."));

    }




    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.bind(this);
        forceRTLIfSupported();

    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void forceRTLIfSupported()
    {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LOCALE);
        }
    }


//	@Override
//	public boolean dispatchKeyEvent(KeyEvent event) {
//		Log.e("keyevent","dispatchKeyEvent");
//		final int keyCode = event.getKeyCode();
//		final int action = event.getAction();
//
//		if (keyCode == KeyEvent.KEYCODE_MENU && action == KeyEvent.ACTION_DOWN) {
//			// do your stuff
//		}
//
//		return super.dispatchKeyEvent(event);
//	}



    protected void toggleAppBarLayoutState(AppBarLayout appBarLayout, boolean state) {
        if (appBarLayout != null && ViewCompat.isLaidOut(appBarLayout)) {
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) appBarLayout.getLayoutParams();
            AppBarLayout.Behavior behavior = (AppBarLayout.Behavior) params.getBehavior();

            behavior.setDragCallback(state ? enabledDragCallback : disabledDragCallback);

        }
    }
}
