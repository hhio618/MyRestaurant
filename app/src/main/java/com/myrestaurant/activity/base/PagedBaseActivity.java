package com.myrestaurant.activity.base;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.myrestaurant.R;


/**
 * Created by vortex on 1/4/15.
 */
public abstract class PagedBaseActivity extends BaseActivity implements TabLayout.OnTabSelectedListener, ViewPager.OnPageChangeListener {


    public static final ViewPager.PageTransformer NO_OP_PAGE_TRANSFORMER = new ViewPager.PageTransformer() {
        @Override
        public void transformPage(View page, float position) {

        }
    };
    private ViewPager mPager;
    private FragmentStatePagerAdapter adapter;
    private boolean isPagerEnable = true;

    public ViewPager getViewPager() {
        return mPager;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (isPagerEnable) {
            reInitializePagerActivity();
        }
    }

    protected void reInitializePagerActivity() {
        mPager = (ViewPager) findViewById(R.id.pager);
        final TabLayout tabHost = (TabLayout) findViewById(R.id.tabs);

        onCreateViewPager(mPager, tabHost);
    }

    public boolean isPagerEnable() {
        return isPagerEnable;
    }

    public void setPagerEnable(boolean isPagerEnable) {
        this.isPagerEnable = isPagerEnable;
    }

    protected abstract void onViewPagerCreated(ViewPager pager, TabLayout tabHost);

    protected void onTabCreated(TabLayout indicator) {


    }

    private void onCreateViewPager(final ViewPager pager, final TabLayout tabHost) {
        this.adapter = onCreatePagerAdapter();

        pager.setAdapter(adapter);
        pager.setOffscreenPageLimit(adapter.getCount());
        pager.setPageTransformer(false, NO_OP_PAGE_TRANSFORMER);
        tabHost.setupWithViewPager(mPager);
        onTabCreated(tabHost);
        tabHost.setOnTabSelectedListener(this);
        pager.post(new Runnable() {
            @Override
            public void run() {
                onViewPagerCreated(pager, tabHost);
            }
        });


    }

    protected abstract FragmentStatePagerAdapter onCreatePagerAdapter();


    @Override
    protected void onStart() {
        super.onStart();
//        new Handler().post(new Runnable() {
//            @Override
//            public void run() {
//                for (int i = 0 ; i < adapter.getCount(); i++) {
//                    Fragment frg = adapter.getItem(i);
//                    if (frg instanceof Reloadable)
//                        ((Reloadable) frg).reload();
//                }
//            }
//        });

    }

    public FragmentStatePagerAdapter getAdapter() {
        return adapter;
    }




    @Override
    public void onPageScrolled(int i, float v, int i2) {

    }

    @Override
    public void onPageSelected(int i) {

    }

    @Override
    public void onPageScrollStateChanged(int i) {
        toggleAppBarLayoutState(appBarLayout, i == ViewPager.SCROLL_STATE_IDLE);
    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
//        Fragment reloadable = getAdapter().getCurrentFragment(position);
//        if (reloadable instanceof CustomRecyclerFragment) {
//            CustomRecyclerFragment recyclerFragment = ((CustomRecyclerFragment) reloadable);
//            if (recyclerFragment.getRecyclerView().computeVerticalScrollOffset() < 10) {
//                if (reloadable instanceof Reloadable)
//                    ((Reloadable) reloadable).reload();
//            } else
//                recyclerFragment.getRecyclerView().scrollToPosition(0);
//
//        }

    }
}
