package com.myrestaurant.activity.base;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.myrestaurant.R;

import rx.Subscription;

/**
 * Created by hhio618 on 3/9/16.
 */
public abstract class BaseDrawerActivity extends BaseActivity {

    private DrawerLayout mDrawerLayout;
    /* (non-Javadoc)
         * @see android.app.Activity#onKeyUp(int, android.view.KeyEvent)
         */
    private boolean isDrawerOpened;
    private NavigationView navigationView;
    private int navCheckedItem = -1;
    private Subscription profileManagerSubscription;
    private ActionBarDrawerToggle actionBarDrawerToggle;


    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void setContentView(int layoutResID) {
        mDrawerLayout = (DrawerLayout) getLayoutInflater().inflate(R.layout.activity_base_layout_drawer, null);
        CoordinatorLayout coordinatorLayout = (CoordinatorLayout) mDrawerLayout.findViewById(R.id.main_content);

        getLayoutInflater().inflate(layoutResID, coordinatorLayout, true);
        super.setContentView(mDrawerLayout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
//        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
        actionBar.setDisplayHomeAsUpEnabled(true);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            mDrawerLayout.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        }
        actionBarDrawerToggle = new ActionBarDrawerToggle(this,mDrawerLayout,R.string.open_drawer,R.string.close_drawer);

        mDrawerLayout.addDrawerListener(actionBarDrawerToggle);
        if (navigationView != null) {

            setupDrawerContent();
        }

        // Update the action bar info with the TypefaceSpan instance


//        final EditText query = (EditText) mDrawerLayout.findViewById(R.id.search_textedit);
//        mDrawerLayout.findViewById(R.id.menu_search).setOnClickListener(this);
//        query.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
//                    Toast.makeText(DrawerActivity.this, "" + query.getText().toString(), Toast.LENGTH_LONG).show();
//                    performSearch(query.getText().toString());
//                    return true;
//                }
//                return false;
//            }
//        });


//		setSupportActionBar(toolbar);
//		setTitle("Activity Title");
    }

    @Override protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        actionBarDrawerToggle.syncState();
    }

    @Override protected void onResume() {
        super.onResume();
        if (navCheckedItem != -1)
            navigationView.setCheckedItem(navCheckedItem);
    }


    private void setupDrawerContent() {

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override public boolean onNavigationItemSelected(MenuItem item) {
                item.setChecked(true);
                mDrawerLayout.closeDrawers();
                return true;
            }
        });
    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }




}
