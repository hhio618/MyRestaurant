package com.myrestaurant.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.myrestaurant.Constants;
import com.myrestaurant.R;
import com.myrestaurant.activity.base.PagedBaseActivity;
import com.myrestaurant.fragment.DetailsFragment;
import com.myrestaurant.model.Food;
import com.myrestaurant.rxbus.RxBus;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import rx.functions.Action1;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.TypefaceUtils;

/**
 * Created by vortex on 7/2/16.
 */
public class DetailsActivity extends PagedBaseActivity implements Action1<Food> {

    @BindView(R.id.food_icon_app_bar) ImageView food_icon;
    @BindView(R.id.fab_more) FloatingActionButton fabMore;
    @BindView(R.id.collapsing_toolbar) CollapsingToolbarLayout collapsingToolbarLayout;
    private Parcelable foodParcelable;

    @Optional
    @OnClick(R.id.fab_more)
    public void onFABMoreClicked()
    {
        if(foodParcelable != null){
            Intent intent = new Intent(this, WebViewActivity.class);
            intent.putExtra(Constants.FOOD_OBJECT, foodParcelable);
            startActivity(intent);
        }
    }
    @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        if(savedInstanceState != null)
            foodParcelable = savedInstanceState.getParcelable(Constants.FOOD_OBJECT);
        ButterKnife.bind(this);
        Toolbar toolbar = ButterKnife.findById(this,R.id.toolbar);
        setSupportActionBar(toolbar);


        //noinspection ConstantConditions
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Typeface typeface = TypefaceUtils.load(getAssets(), CalligraphyConfig.get().getFontPath());
        collapsingToolbarLayout.setCollapsedTitleTypeface(typeface);
        collapsingToolbarLayout.setExpandedTitleTypeface(typeface);
    }


    @Override protected void onStart() {
        super.onStart();
        RxBus.getInstance().register(Food.class, this, this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(Constants.FOOD_OBJECT, foodParcelable);
        super.onSaveInstanceState(outState);
    }

    @Override protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                // This ID represents the Home or Up button. In the case of this
                // activity, the Up button is shown. Use NavUtils to allow users
                // to navigate up one level in the application structure. For
                // more details, see the Navigation pattern on Android Design:
                //
                // http://developer.android.com/design/patterns/navigation.html#up-vs-back
                //

                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Nullable @Override public Intent getParentActivityIntent() {
        Intent intent =  super.getParentActivityIntent();
        if(intent != null)
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }


    @Nullable
    @Override
    public Intent getSupportParentActivityIntent() {
        Intent intent =  super.getSupportParentActivityIntent();
        if(intent != null)
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    @Override
    protected void onViewPagerCreated(ViewPager pager, TabLayout tabHost) {

    }

    @Override
    protected FragmentStatePagerAdapter onCreatePagerAdapter() {
        return new FragmentStatePagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                Fragment fragment = null;
                switch (position){
                    case 0:
                        Bundle inputBundle = getIntent().getExtras();
                        fragment = new DetailsFragment();
                        fragment.setArguments(inputBundle);
                        break;
                }
                return fragment;
            }

            @Override public CharSequence getPageTitle(int position) {
                CharSequence title = null;
                switch (position){
                    case 0:
                        title = DetailsFragment.getName(getApplicationContext());
                }
                return title;
            }

            @Override
            public int getCount() {
                return 1;
            }
        };
    }

    @Override public void call(Food food) {
        Glide.with(this)
                .load(food.getIcon())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(food_icon);
        collapsingToolbarLayout.setTitle(food.getFood_name());
        foodParcelable = Parcels.wrap(food);
    }
}
