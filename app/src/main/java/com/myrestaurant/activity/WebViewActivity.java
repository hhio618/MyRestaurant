package com.myrestaurant.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.NavUtils;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.myrestaurant.Constants;
import com.myrestaurant.R;
import com.myrestaurant.activity.base.BaseActivity;
import com.myrestaurant.fragment.WebViewFragment;

import butterknife.ButterKnife;

/**
 * Created by vortex on 7/3/16.
 */
public class WebViewActivity extends BaseActivity {

    //persistable bundle save state across restart
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        Toolbar toolbar = ButterKnife.findById(this,R.id.toolbar);
        setSupportActionBar(toolbar);
        //noinspection ConstantConditions
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportFragmentManager().beginTransaction().replace(R.id.container, WebViewFragment.newInstance(getIntent().getExtras())
                , "webViewFragment").commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // This ID represents the Home or Up button. In the case of this
                // activity, the Up button is shown. Use NavUtils to allow users
                // to navigate up one level in the application structure. For
                // more details, see the Navigation pattern on Android Design:
                //
                // http://developer.android.com/design/patterns/navigation.html#up-vs-back
                //
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Nullable @Override public Intent getParentActivityIntent() {
        Intent intent =  super.getParentActivityIntent();
        if(intent != null){
            if (NavUtils.shouldUpRecreateTask(this, intent)) {
                intent.putExtra(Constants.FOOD_OBJECT, getIntent().getExtras().getParcelable(Constants.FOOD_OBJECT));
            } else {
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            }
        }
        return intent;
    }

    @Nullable
    @Override
    public Intent getSupportParentActivityIntent() {
        Intent intent =  super.getSupportParentActivityIntent();
        if(intent != null){
            if (NavUtils.shouldUpRecreateTask(this, intent)) {
                intent.putExtra(Constants.FOOD_OBJECT, getIntent().getExtras().getParcelable(Constants.FOOD_OBJECT));
            } else {
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            }
        }
        return intent;
    }

}
