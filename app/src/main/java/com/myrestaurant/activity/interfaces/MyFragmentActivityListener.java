package com.myrestaurant.activity.interfaces;

import com.myrestaurant.model.Food;

/**
 * Created by hhio618 on 7/4/16.
 */
public interface MyFragmentActivityListener {
     void createDetailsFragment(Food food);
     void setFoodFromMainFragmentItem(Food food);
}
