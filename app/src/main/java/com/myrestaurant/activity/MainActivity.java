package com.myrestaurant.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.view.View;

import com.myrestaurant.Constants;
import com.myrestaurant.R;
import com.myrestaurant.activity.base.BaseDrawerActivity;
import com.myrestaurant.event.CreateDetailsFragmentEvent;
import com.myrestaurant.exceptions.FloatingButtonClickedException;
import com.myrestaurant.fragment.DetailsFragment;
import com.myrestaurant.rxbus.RxBus;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import rx.functions.Action1;

public class MainActivity extends BaseDrawerActivity implements Action1<CreateDetailsFragmentEvent> {
    private static final String ACTIVITY_TAG = "MainActivity";
    @BindView(R.id.drawer_layout) DrawerLayout mDrawerLayout;
    @BindView(R.id.nav_view) NavigationView navigationView;
    @Nullable
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @Nullable
    @BindView(R.id.fab_more)
    FloatingActionButton fabMore;
    private Parcelable foodParcelable;

    @Optional
    @OnClick(R.id.fab_more)
    public void onFABMoreClicked()
    {
        if(foodParcelable != null){
            Intent intent = new Intent(this, WebViewActivity.class);
            intent.putExtra(Constants.FOOD_OBJECT, foodParcelable);
            startActivity(intent);
        }
    }
    @Optional
    @OnClick(R.id.fab)
    public void onFABClicked(View v)
    {
        try {
            testException();
        } catch (FloatingButtonClickedException|IOException e) {
            Snackbar.make(v,"A test text!",Snackbar.LENGTH_LONG).show();
        }
//                try {
//                    Thread.sleep(3000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        if(savedInstanceState != null)
            foodParcelable = savedInstanceState.getParcelable(Constants.FOOD_OBJECT);

//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

//        final ActionBar ab = getSupportActionBar();
//        ab.setHomeAsUpIndicator(R.drawable.ic_menu);
//        ab.setDisplayHomeAsUpEnabled(true);
//        navigationView.


/*        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container,new MainFragment());
        fragmentTransaction.commit();*/
//        getSupportFragmentManager().beginTransaction().replace(R.id.container,new MainFragment(),"mainFragment").commit();

    }

    public void testException() throws FloatingButtonClickedException, IOException {
            throw new FloatingButtonClickedException("khata");
    }

    @Override protected void onStart() {
        super.onStart();
        RxBus.getInstance().register(CreateDetailsFragmentEvent.class,this,this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(Constants.FOOD_OBJECT, foodParcelable);
        super.onSaveInstanceState(outState);
    }



//
//    @Override public void createDetailsFragment(Food food) {
////        DetailsFragment detailsFragment = new DetailsFragment();
////        Bundle bundle = new Bundle();
////        bundle.putParcelable(Constants.FOOD_OBJECT, Parcels.wrap(food));
////        detailsFragment.setArguments(bundle);
//        getSupportFragmentManager().beginTransaction().
//                replace(R.id.details_container,  DetailsFragment.newInstance(food),"detailsFragment")
//                .commit();
//    }

//    @Override public void setFoodFromMainFragmentItem(Food food) {
//        DetailsFragment detailsFragment = (DetailsFragment) getSupportFragmentManager().findFragmentByTag("detailsFragment");
//        if (detailsFragment != null)
//            detailsFragment.updateFragment(food);
//        else
//            createDetailsFragment(food);
//    }

    @Override public void call(CreateDetailsFragmentEvent createDetailsFragment) {
        getSupportFragmentManager().beginTransaction().
                replace(R.id.details_container, DetailsFragment.newInstance(createDetailsFragment.getFood()),"detailsFragment")
                .commit();
    }
}
