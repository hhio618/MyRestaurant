package com.myrestaurant.view.abs;

import android.support.v7.widget.RecyclerView;

/**
 * Created by hhio618 on 7/21/16.
 */
public interface RecyclerRow {
    int getItemType();
    void bind(RecyclerView.ViewHolder viewHolder,Object... args);
}
