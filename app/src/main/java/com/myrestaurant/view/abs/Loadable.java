package com.myrestaurant.view.abs;

import com.myrestaurant.view.delegate.LoadingManagerDelegate;

/**
 * Created by hhio618 on 7/21/16.
 */
public interface Loadable extends Contentable {
    void onRequestLoadMore(int page);
    void onRequestReload();
    LoadingManagerDelegate getLoadingManagerDelegate();

}
