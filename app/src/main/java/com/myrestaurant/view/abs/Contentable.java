package com.myrestaurant.view.abs;

/**
 * Created by hhio618 on 7/21/16.
 */
public interface Contentable {
    void showEmptyView();

    void showErrorView();

    void showLoadingView();

    void showContentView();


}
