package com.myrestaurant.view.delegate;

import android.support.annotation.NonNull;

import com.myrestaurant.view.abs.Loadable;
import com.myrestaurant.view.delegate.enums.LoadingResult;

/**
 * Created by hhio618 on 7/23/16.
 */
public class LoadingManagerDelegate {
    private final Loadable loadable;

    private static enum Loading {
        IDLE, RELOAD, LOAD_MORE
    }

    private Loading loading = Loading.IDLE;

    public LoadingManagerDelegate(Loadable loadable) {
        this.loadable = loadable;
    }

    public synchronized void doLoadMore(int page) {
        if (loading == Loading.IDLE) {
            loading = Loading.LOAD_MORE;
            loadable.onRequestLoadMore(page);

        }
    }

    public synchronized void doReload() {
        if (loading == Loading.IDLE)
            loading = Loading.RELOAD;
            loadable.showLoadingView();
            loadable.onRequestReload();
    }

    public synchronized void finishLoading(@NonNull LoadingResult loadingResult) {

        loading = Loading.IDLE;
        switch (loadingResult){
            case OK:
                loadable.showContentView();
                break;
            case EMPTY:
                loadable.showEmptyView();
                break;
            case ERROR:
                loadable.showErrorView();
                break;
            default:
                break;
        }

    }
}
