package com.myrestaurant.view.delegate.enums;

/**
 * Created by hhio618 on 7/24/16.
 */
public enum LoadingResult {
    OK, EMPTY, ERROR
}
