package com.myrestaurant.view.adapter;

import android.support.v7.widget.RecyclerView;

import com.myrestaurant.view.abs.RecyclerRow;

import java.util.List;

/**
 * Created by hhio618 on 7/23/16.
 */
public abstract class EnhancedAdapter<T extends RecyclerRow> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final int DEFAULT = 0;
    private final List<T> objectList;

    public List<T> getItems() {
        return objectList;
    }

    public  EnhancedAdapter(List<T> objectList){
        this.objectList = objectList;
    }

    @Override public int getItemCount() {
        return objectList.size();
    }

    @Override public int getItemViewType(int position) {
        return objectList.get(position).getItemType();
    }
}
