package com.myrestaurant.view.row;

import com.myrestaurant.view.abs.RecyclerRow;

/**
 * Created by hhio618 on 7/23/16.
 */
public abstract class RecyclerRowImpl<T> implements RecyclerRow {

    protected final T data;

    public T getData() {
        return data;
    }

    public RecyclerRowImpl(T data) {
        this.data = data;
    }


}
