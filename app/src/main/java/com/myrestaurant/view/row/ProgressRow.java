package com.myrestaurant.view.row;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.myrestaurant.Constants;
import com.myrestaurant.R;

/**
 * Created by hhio618 on 7/23/16.
 */
public class ProgressRow extends RecyclerRowImpl<Void>{


    public ProgressRow(Void data) {
        super(data);
    }


    public static class ProgressHolder extends RecyclerView.ViewHolder{

        public ProgressHolder(ViewGroup viewGroup) {
            super(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.progress_row,viewGroup, false));
        }
    }

    @Override public int getItemType() {
        return Constants.RecyclerViewType.PROGRESS.ordinal();
    }

    @Override public void bind(RecyclerView.ViewHolder viewHolder, Object... args) {

    }


}
