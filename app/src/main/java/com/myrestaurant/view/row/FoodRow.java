package com.myrestaurant.view.row;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.myrestaurant.Constants;
import com.myrestaurant.R;
import com.myrestaurant.activity.DetailsActivity;
import com.myrestaurant.model.Food;
import com.myrestaurant.rxbus.RxBus;

import org.parceler.Parcels;

/**
 * Created by hhio618 on 7/23/16.
 */
public class FoodRow extends RecyclerRowImpl<Food> {


    public FoodRow(Food data) {
        super(data);
    }

    public static class FoodHolder extends RecyclerView.ViewHolder{
        public final TextView food_name;
        public final TextView food_desc;
        public final ImageView food_icon;
        public FoodHolder(ViewGroup viewGroup) {
            super(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.food_row,viewGroup, false));
            food_name = (TextView) itemView.findViewById(R.id.food_name);
            food_desc = (TextView) itemView.findViewById(R.id.food_desc);
            food_icon = (ImageView) itemView.findViewById(R.id.food_icon);
        }
    }

    @Override public int getItemType() {
        return Constants.RecyclerViewType.DEFAULT.ordinal();
    }

    @Override public void bind(RecyclerView.ViewHolder holder, Object... args) {
        final FoodHolder viewHolder = (FoodHolder) holder;
        viewHolder.food_name.setText(data.getFood_name());
        viewHolder.food_desc.setText(data.getFood_description());
//            WeakReference<ImageView> imageViewWeakReference = new WeakReference<ImageView>(holder.viewHolder.food_icon);
//            new DownloadImageTask(imageViewWeakReference).execute(food.getIcon());
        ((RequestManager)args[0])
                .load(data.getIcon())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.placeholder)
                .crossFade()
                .into(viewHolder.food_icon);
// holder.viewHolder.food_icon.setImageResource(food.getIcon());
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewHolder.itemView.getContext().getResources().getBoolean(R.bool.is_tablet)) {
                    //3rd way (new)
                    RxBus.getInstance().post(data);
                } else {
                    Intent detailsIntent = new Intent(viewHolder.itemView.getContext(), DetailsActivity.class);
/*
############## codi ke sar class goftam ke doroste vali sar api 16 be bala kar mikone:
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.viewHolder.food_NAME, food.getviewHolder.food_name());
                    bundle.putString(Constants.viewHolder.food_DESC, food.getviewHolder.food_description());
                    bundle.putInt(Constants.viewHolder.food_ICON, food.getIcon());
//                    bundle.putSerializable(Constants.viewHolder.food_OBJECT, food);
                    startActivity(detailsIntent, bundle);
*/
//############ codi ke dorostare va baray tamam api ha hast
//                        detailsIntent.putExtra(Constants.viewHolder.food_NAME, food.getviewHolder.food_name());
//                        detailsIntent.putExtra(Constants.viewHolder.food_DESC, food.getviewHolder.food_description());
//                        detailsIntent.putExtra(Constants.viewHolder.food_ICON, food.getIcon());
                    detailsIntent.putExtra(Constants.FOOD_OBJECT, Parcels.wrap(data));
                    viewHolder.itemView.getContext().startActivity(detailsIntent);
                }

            }
        });
    }


}
