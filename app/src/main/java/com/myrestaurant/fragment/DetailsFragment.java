package com.myrestaurant.fragment;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.PopupMenu;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.myrestaurant.Constants;
import com.myrestaurant.R;
import com.myrestaurant.fragment.base.BaseFragment;
import com.myrestaurant.model.Food;
import com.myrestaurant.repository.MyApiEndpointInterface;
import com.myrestaurant.repository.RetrofitRestClient;
import com.myrestaurant.rxbus.RxBus;

import org.parceler.Parcels;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.functions.Action1;

/**
 * Created by vortex on 7/3/16.
 */
public class DetailsFragment extends BaseFragment implements Action1<Food> {
    private static final int REQUEST_STORAGE_WRITE_ACCESS_PERMISSION = 1000;
    private Food food;
    @BindView(R.id.food_name)
    TextView food_name;
    @BindView(R.id.food_desc)
    TextView food_desc;
    @Nullable
    @BindView(R.id.food_icon)
    ImageView food_icon;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle inputBundle = getArguments();
        if (inputBundle != null) {
            food = Parcels.unwrap(inputBundle.getParcelable(Constants.FOOD_OBJECT));

        }
//niaz nist chone argumante fragment khodesh gabeliat save va restore dare
//        if (savedInstanceState != null)
//            food = Parcels.unwrap(savedInstanceState.getParcelable(Constants.FOOD_OBJECT));

    }

//niaz nist chone argumante fragment khodesh gabeliat save va restore dare
//    @Override
//    public void onSaveInstanceState(Bundle outState) {
//        outState.putParcelable(Constants.FOOD_OBJECT, Parcels.wrap(food));
//        super.onSaveInstanceState(outState);
//    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup
            container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_details, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Uri uri = getActivity().getIntent().getData();
        if (uri != null) {
            //TODO show loading
            final int id = Integer.valueOf(uri.getQueryParameter("id"));
            MyApiEndpointInterface endpointInterface = RetrofitRestClient.getInstance();
            endpointInterface.getFoods().enqueue(new Callback<List<Food>>() {
                @Override
                public void onResponse(Call<List<Food>> call, Response<List<Food>> response) {
                    List<Food> foods = response.body();
                    if (response.body() != null) {
                        for (Food food : foods) {
                            if (food.getId() == id) {
                                getArguments().putParcelable(Constants.FOOD_OBJECT, Parcels.wrap(food));
                                //TODO hide loading
                                bindDataToLayout();
                            }
                        }
                    }

                }

                @Override
                public void onFailure(Call<List<Food>> call, Throwable t) {
                    if (getView() != null)
                        Snackbar.make(getView(), R.string.api_list_load_failed, Snackbar.LENGTH_INDEFINITE).show();
                }
            });
        } else
            bindDataToLayout();
    }

    @Override
    public void onStart() {
        super.onStart();
        //For 3rd way
        RxBus.getInstance().register(Food.class, this, this);
    }

    public void bindDataToLayout() {
        if (getArguments() != null) {
            food = Parcels.unwrap(getArguments().getParcelable(Constants.FOOD_OBJECT));

            if (food != null) {
                food_name.setText(food.getFood_name());
                food_desc.setText(food.getFood_description());
//        food_icon.setImageResource(food.getIcon());
                if (food_icon != null) {
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
//                        food_icon.setImageDrawable(getResources().getDrawable(food.getIcon(), getActivity().getTheme()));
//                    else
//                        food_icon.setImageDrawable(getResources().getDrawable(food.getIcon()));
                    Glide.with(this)
                            .load(food.getIcon())
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(food_icon);

                    food_icon.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
/*
final ListPopupWindow listPopupWindow = new ListPopupWindow(getContext());
                        HashMap<String, String> hashMap = new HashMap<String, String>();
                        hashMap.put("title", getString(R.string.save_image));
                        List<HashMap<String, String>> hashMaps = new ArrayList<HashMap<String, String>>();

                        listPopupWindow.setAdapter(new SimpleAdapter(getActivity(),
                                hashMaps, android.R.layout.simple_list_item_1,
                                new String[]{"title"}, new int[]{android.R.id.text1}));
                        listPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN // Permission was added in API Level 16
                                        && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                        != PackageManager.PERMISSION_GRANTED) {
                                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_STORAGE_WRITE_ACCESS_PERMISSION);
                                } else {

                                    saveImageAndShowNotification();


                                }
                            }
                        });
                        listPopupWindow.setWidth(400);
                        listPopupWindow.setHeight(400);
                        listPopupWindow.setModal(true);
                        listPopupWindow.setAnchorView(v);
                        listPopupWindow.show();
                            */
                            PopupMenu popupMenu = new PopupMenu(getContext(), v, Gravity.CENTER_HORIZONTAL);
//                        popupMenu.getMenuInflater().inflate(R.menu.save_image_popup,popupMenu.getMenu());
                            popupMenu.getMenu().add(Menu.NONE, R.id.save_image, 0, getString(R.string.save_image));
                            popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                                @Override
                                public boolean onMenuItemClick(MenuItem item) {
                                    switch (item.getItemId()) {
                                        case R.id.save_image:
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN // Permission was added in API Level 16
                                                    && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                                    != PackageManager.PERMISSION_GRANTED) {
                                                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_STORAGE_WRITE_ACCESS_PERMISSION);
                                            } else {

                                                saveImageAndShowNotification();


                                            }
                                            break;
                                    }
                                    return true;
                                }
                            });
                            popupMenu.show();


                        }
                    });
                }else {
                    RxBus.getInstance().post(food);
                }


            }
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_STORAGE_WRITE_ACCESS_PERMISSION:
                if (grantResults[0] == android.content.pm.PackageManager.PERMISSION_GRANTED) {
                    saveImageAndShowNotification();
                }
                break;
            default:
                break;
        }
    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    private void saveImageAndShowNotification() {
        Glide.with(this)
                .load(food.getIcon())
                .asBitmap()
                .toBytes(Bitmap.CompressFormat.JPEG, 90)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(new SimpleTarget<byte[]>() {
                    @Override
                    public void onResourceReady(byte[] resource, GlideAnimation<? super byte[]> glideAnimation) {
                        saveImage(resource);
                        notifyUser();
                    }
                });

    }



    private void notifyUser() {
        if (getView() != null) {
            final Snackbar snackbar = Snackbar.make(getView(), getString(R.string.photo_saved), Snackbar.LENGTH_INDEFINITE);
            snackbar.setAction("Hide", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //TODO action
                    snackbar.dismiss();
                    ;
                }
            })
                    .show();
        }
    }

    private void saveImage(byte[] bytes) {

        if (isExternalStorageWritable()) {
            File root = getActivity().getExternalFilesDir(null);
            File myDir = new File(root, "saved_images");
            myDir.mkdirs();
            Random generator = new Random();
            int n = 10000;
            n = generator.nextInt(n);
            String fname = "image-" + n + ".jpg";
            File file = new File(myDir, fname);
            if (file.exists()) file.delete();
            try {
                FileOutputStream out = new FileOutputStream(file);
                out.write(bytes);
                out.flush();
                out.close();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    //For 3rd way
    @Override
    public void call(Food food) {
        getArguments().putParcelable(Constants.FOOD_OBJECT, Parcels.wrap(food));
        bindDataToLayout();
    }

    //for 1st & 2nd way
    public void updateFragment(Food food) {
        getArguments().putParcelable(Constants.FOOD_OBJECT, Parcels.wrap(food));
        bindDataToLayout();
    }

    public static Fragment newInstance(Food food) {
        DetailsFragment detailsFragment = new DetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.FOOD_OBJECT, Parcels.wrap(food));
        detailsFragment.setArguments(bundle);
        return detailsFragment;
    }

    public static CharSequence getName(Context context) {
        return context.getString(R.string.fragment_title_details);
    }

}
