package com.myrestaurant.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.myrestaurant.Constants;
import com.myrestaurant.R;
import com.myrestaurant.fragment.base.BaseFragment;
import com.myrestaurant.fragment.dialogs.LoadingDialogFragment;
import com.myrestaurant.model.Food;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hhio618 on 7/4/16.
 */
public class WebViewFragment extends BaseFragment {
    @BindView(R.id.webView)
    WebView webView;
    @Nullable @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_webview,container, false);
    }

    @Override public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Food food = Parcels.unwrap(getArguments().getParcelable(Constants.FOOD_OBJECT));
        String url = (food == null ? null:food.getExtra_url());
        showPleaseWaitDialog();

        webView.loadUrl(url);
        webView.setWebViewClient(new WebViewClient(){
            @Override public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if(getView() != null)
                    hidePleaseWaitDialog();
            }
        });

    }

    private void showPleaseWaitDialog(){
        new LoadingDialogFragment().show(getChildFragmentManager(), "pleaseWait");
    }

    private void hidePleaseWaitDialog(){
        //ghable hide kardan bayad check konim fragment vojod dashte bashe badesh hide konim chon momkene
        //ghablesh karbar click karde bashe kharej dialog va dialog rafte bashe badesh onPage finished ejra beshe
        Fragment fragment = getChildFragmentManager().findFragmentByTag("pleaseWait");
        if(fragment != null)
            getChildFragmentManager().beginTransaction().remove(fragment).commit();

    }
    public static Fragment newInstance(Bundle extras) {
        Fragment frg = new WebViewFragment();
        frg.setArguments(extras);
        return frg;
    }
}
