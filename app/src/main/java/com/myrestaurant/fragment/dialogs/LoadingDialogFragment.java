package com.myrestaurant.fragment.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.myrestaurant.R;
import com.myrestaurant.fragment.base.BaseDialogFragment;

/**
 * Created by hhio618 on 7/4/16.
 */
public class LoadingDialogFragment extends BaseDialogFragment {

    @NonNull @Override public Dialog onCreateDialog(Bundle savedInstanceState) {
//        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
//                .setTitle("Loading").setMessage("Please Wait!");
//
//
//        return builder.create();
        AppCompatDialog appCompatDialog = new AppCompatDialog(getActivity());
        appCompatDialog.setContentView(R.layout.dialog_loading);
        appCompatDialog.setCancelable(true);
        return appCompatDialog;
//        return super.onCreateDialog(savedInstanceState);
    }



    @Nullable @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_loading,container, false);
    }
}
