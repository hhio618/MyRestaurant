package com.myrestaurant.fragment.base;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.myrestaurant.R;
import com.myrestaurant.view.ContentLoaderFrameLayout;
import com.myrestaurant.view.abs.Loadable;
import com.myrestaurant.view.delegate.LoadingManagerDelegate;

import butterknife.BindView;

/**
 * Created by vortex on 7/13/16.
 */
public abstract class BaseContentFragment extends BaseFragment implements Loadable, SwipeRefreshLayout.OnRefreshListener {
    @BindView(R.id.refresh_layout) SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.content_frame) ContentLoaderFrameLayout contentLoaderFrameLayout;
    private LoadingManagerDelegate loadingManagerDelegate;

    @Override
    public LoadingManagerDelegate getLoadingManagerDelegate() {
        return loadingManagerDelegate;
    }

    public ContentLoaderFrameLayout getContentLoaderFrameLayout() {
        return contentLoaderFrameLayout;
    }

    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return swipeRefreshLayout;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadingManagerDelegate = new LoadingManagerDelegate(this);

    }

    @Override public void onDestroy() {
        loadingManagerDelegate = null;
        super.onDestroy();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_load, container, false);

    }


    @Override public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refresh_layout);
        contentLoaderFrameLayout = (ContentLoaderFrameLayout) view.findViewById(R.id.content_frame);
        swipeRefreshLayout.setOnRefreshListener(this);
        onCreateLoadFrameLayout(contentLoaderFrameLayout, savedInstanceState);

    }

    protected abstract void onCreateLoadFrameLayout(@NonNull ContentLoaderFrameLayout contentLoaderFrameLayout, @Nullable Bundle savedInstanceState);

    @Override public synchronized void showEmptyView() {
        getContentLoaderFrameLayout().showEmptyView();
    }

    @Override public synchronized void showErrorView() {
        getContentLoaderFrameLayout().showErrorView();
    }

    @Override public synchronized void showLoadingView() {
        getContentLoaderFrameLayout().showLoadingView();
    }

    @Override public synchronized void showContentView() {
        getContentLoaderFrameLayout().showContentView();
    }


}
