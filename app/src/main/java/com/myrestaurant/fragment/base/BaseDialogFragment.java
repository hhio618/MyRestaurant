package com.myrestaurant.fragment.base;

import com.trello.rxlifecycle.components.support.RxAppCompatDialogFragment;

/**
 * Created by hhio618 on 7/4/16.
 */
public abstract class BaseDialogFragment extends RxAppCompatDialogFragment {
}
