package com.myrestaurant.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.myrestaurant.R;
import com.myrestaurant.event.CreateDetailsFragmentEvent;
import com.myrestaurant.fragment.base.BaseContentFragment;
import com.myrestaurant.model.Food;
import com.myrestaurant.repository.RetrofitRestClient;
import com.myrestaurant.rxbus.RxBus;
import com.myrestaurant.view.ContentLoaderFrameLayout;
import com.myrestaurant.view.abs.RecyclerRow;
import com.myrestaurant.view.adapter.EnhancedAdapter;
import com.myrestaurant.view.callback.EndlessRecyclerViewScrollListener;
import com.myrestaurant.view.decoration.DividerItemDecoration;
import com.myrestaurant.view.delegate.enums.LoadingResult;
import com.myrestaurant.view.row.FoodRow;
import com.myrestaurant.view.row.ProgressRow;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func0;
import rx.functions.Func1;

/**
 * Created by vortex on 7/3/16.
 */
public class MainFragment extends BaseContentFragment{
    @BindView(R.id.recycler_view) RecyclerView recyclerView;
    private final Callback<List<Food>> reloadCallback = new Callback<List<Food>>() {
        @Override
        public void onResponse(Call<List<Food>> call, Response<List<Food>> response) {
            final List<Food> foods = response.body();
            if (foods != null) {
                LoadingResult loadingResult;
                if(foods.size() > 0) {
                    Observable.defer(new Func0<Observable<Food>>() {
                        @Override public Observable<Food> call() {
                            return Observable.from(foods);
                        }
                    }).map(new Func1<Food, RecyclerRow>() {

                        @Override public RecyclerRow call(Food food) {
                            return new FoodRow(food);
                        }
                    }).toList().subscribe(new Action1<List<RecyclerRow>>() {
                        @Override public void call(List<RecyclerRow> recyclerRows) {

                            getAdapter().getItems().clear();
                            getAdapter().getItems().addAll(recyclerRows);
                            getAdapter().notifyDataSetChanged();
                        }
                    });


//                    RequestManager requestManager = Glide.with(MainFragment.this);
//                    preloadSizeProvider = new ViewPreloadSizeProvider<>();
//                    RecyclerViewPreloader<Food> preloader = new RecyclerViewPreloader<>(requestManager, recyclerView.getAdapter(),
//                            preloadSizeProvider, PRELOAD_AHEAD_ITEMS);
//                    recyclerView.addOnScrollListener(preloader);
                    if (getResources().getBoolean(R.bool.is_tablet)) {
                        //2nd way
                        RxBus.getInstance().post(new CreateDetailsFragmentEvent(foods.get(0)));
                    }
                    loadingResult = LoadingResult.OK;
                }else {
                    loadingResult = LoadingResult.EMPTY;
                }
                getLoadingManagerDelegate().finishLoading(loadingResult);
            }



        }

        @Override
        public void onFailure(Call<List<Food>> call, Throwable t) {
//            if (getView() != null)
//                Snackbar.make(getView(), R.string.api_list_load_failed, Snackbar.LENGTH_INDEFINITE).show();
            getLoadingManagerDelegate().finishLoading(LoadingResult.ERROR);
        }
    };

    private final Callback<List<Food>> loadMoreCallback = new Callback<List<Food>>() {
        @Override
        public void onResponse(Call<List<Food>> call, Response<List<Food>> response) {

            List<Food> foods = response.body();
            if (foods != null) {
                LoadingResult loadingResult;
                if(foods.size() > 0) {
                    int last_item = getAdapter().getItemCount()-1;
                    getAdapter().getItems().remove(last_item);
                    for(Food food: foods)
                    {
                        RecyclerRow row = new FoodRow(food);
                        getAdapter().getItems().add(row);
                    }
                    getAdapter().notifyItemRangeInserted(last_item+1,getAdapter().getItemCount());
                    loadingResult = LoadingResult.OK;
                }else {
                    loadingResult = LoadingResult.EMPTY;
                }
                getLoadingManagerDelegate().finishLoading(loadingResult);
            }



        }

        @Override
        public void onFailure(Call<List<Food>> call, Throwable t) {
//            if (getView() != null)
//                Snackbar.make(getView(), R.string.api_list_load_failed, Snackbar.LENGTH_INDEFINITE).show();
            getLoadingManagerDelegate().finishLoading(LoadingResult.EMPTY);
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setRetainInstance(true);
    }

    @Override
    protected void onCreateLoadFrameLayout(@NonNull ContentLoaderFrameLayout contentLoaderFrameLayout, @Nullable Bundle savedInstanceState) {

        contentLoaderFrameLayout.setEmptyView(R.layout.empty_view);
        contentLoaderFrameLayout.setErrorView(R.layout.error_view);
        contentLoaderFrameLayout.setLoadingView(R.layout.loading_view);
        contentLoaderFrameLayout.setContentView(R.layout.frament_main);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }


    @SuppressWarnings("unchecked")
    public EnhancedAdapter<RecyclerRow> getAdapter(){
        return (EnhancedAdapter<RecyclerRow>) recyclerView.getAdapter();
    }
    @Override public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager =
                new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.divider));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override public void onLoadMore(int page, int totalItemsCount) {
                getLoadingManagerDelegate().doLoadMore(page);
            }
        });
        recyclerView.setAdapter(new FoodAdapter(new ArrayList<RecyclerRow>(),Glide.with(this)));
        getLoadingManagerDelegate().doReload();
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @UiThread
    @Override public void onRefresh() {
        getLoadingManagerDelegate().doReload();
        getSwipeRefreshLayout().post(new Runnable() {
            @Override public void run() {
                getSwipeRefreshLayout().setRefreshing(false);
            }
        });
    }



    @Override public void onRequestReload() {
        RetrofitRestClient.getInstance().getFoods().enqueue(reloadCallback);
    }

    @Override public void onRequestLoadMore(int page) {
        getAdapter().getItems().add(new ProgressRow(null));
        getAdapter().notifyItemInserted(getAdapter().getItemCount()-1);
        RetrofitRestClient.getInstance().getFoods().enqueue(loadMoreCallback);
    }

    private class FoodAdapter extends EnhancedAdapter<RecyclerRow> {


        private static final int PROGRESS_ROW = 1;
        private final RequestManager requestManager;



        public FoodAdapter(List<RecyclerRow> recyclerRows, RequestManager requestManager) {
            super(recyclerRows);
            this.requestManager = requestManager;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            switch (viewType){
                case PROGRESS_ROW:
                    return new ProgressRow.ProgressHolder(parent);
                default:
                    return new FoodRow.FoodHolder(parent);

            }
        }

        @Override public void onViewRecycled(RecyclerView.ViewHolder holder) {
            super.onViewRecycled(holder);
            if(holder instanceof FoodRow.FoodHolder)
                Glide.clear(((FoodRow.FoodHolder) holder).food_icon);
        }


        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
           getItems().get(position).bind(holder, requestManager);

        }

    }
}
