package com.myrestaurant.repository;

import com.myrestaurant.model.Food;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

/**
 * Created by vortex on 7/13/16.
 */
public interface MyApiEndpointInterface {
    // Request method and URL specified in the annotation
    // Callback for the parsed response is the last parameter
    @Headers({
            "Cache-Control: max-stale=3600",
            "Content-Type: application/json"
    })
    @GET("578639080f0000b903a5843e") Call<List<Food>> getFoods();


}
