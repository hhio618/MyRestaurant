package com.myrestaurant.repository;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.myrestaurant.BuildConfig;
import com.myrestaurant.RestaurantApplication;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by vortex on 7/13/16.
 */
public class RetrofitRestClient {
    public static final String BASE_URL = "http://www.mocky.io/v2/";
    private static MyApiEndpointInterface INSTANCE = null;

    public static MyApiEndpointInterface getInstance() {
// Trailing slash is needed
        if (INSTANCE == null) {
            File httpCacheDirectory = new File(RestaurantApplication.getContext().getCacheDir(), "responses");
            int cacheSize = 10 * 1024 * 1024; // 10 MiB
            Cache cache = new Cache(httpCacheDirectory, cacheSize);
            OkHttpClient okHttpClient = new OkHttpClient
                    .Builder()
                    .cache(cache)
                    .addNetworkInterceptor(new UserAgentInterceptor(BuildConfig.FLAVOR+":" +BuildConfig.BUILD_TYPE+":" + BuildConfig.VERSION_CODE))
                    .addNetworkInterceptor(CACHE_CONTROL_INTERCEPTOR)
                    .addNetworkInterceptor(new StethoInterceptor())
                    .readTimeout(30, TimeUnit.SECONDS)
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .build();


            INSTANCE = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
//                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                    .create(MyApiEndpointInterface.class);
        }
        return INSTANCE;
    }

    public static final Interceptor CACHE_CONTROL_INTERCEPTOR = new Interceptor() {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();
            // Add Cache Control only for GET methods
            Response response = null;
            if (request.method().equals("GET")) {
                if (!RestaurantApplication.isNetworkConnectionAvailable())  {
                    // 4 weeks stale
                    request = request.newBuilder()
                            .removeHeader("Pragma")
                            .removeHeader("Cache-Control")
                            .header("Cache-Control", "public, only-if-cached, max-stale=" + 60 * 60 * 24 * 7)
                            .build();
                    response = chain.proceed(request);
                }else {
                    if(request.cacheControl().noCache())
                    request = request.newBuilder()
                            .removeHeader("Pragma")
                            .removeHeader("Cache-Control")
                            .header("Cache-Control", "public, max-age=60")
                            .build();
                    //Force response cache
                    response = chain.proceed(request).newBuilder()
                            .header("Cache-Control", "public, max-age=60")
                            .build();
                }
            }


            return response;
        }
    };

    public static final class UserAgentInterceptor implements Interceptor {
        private static final String USER_AGENT_HEADER_NAME = "User-Agent";
        private final String userAgentHeaderValue;

        public UserAgentInterceptor(String userAgentHeaderValue) {
            this.userAgentHeaderValue = userAgentHeaderValue;
            if (this.userAgentHeaderValue == null)
                throw new RuntimeException();
        }

        @Override
        public Response intercept(Chain chain) throws IOException {
            final Request originalRequest = chain.request();
            final Request requestWithUserAgent = originalRequest.newBuilder()
                    .removeHeader(USER_AGENT_HEADER_NAME)
                    .addHeader(USER_AGENT_HEADER_NAME, userAgentHeaderValue)
                    .build();
            return chain.proceed(requestWithUserAgent);
        }
    }


}
