package com.myrestaurant;

import android.util.Log;

/**
 * Created by vortex on 7/2/16.
 */
public class Helper {
    public static void logInformation(String tag, String msg){
        if(BuildConfig.DEBUG)
            Log.i(tag,msg);
    }
}
